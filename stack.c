#include "crpn_incl.h"
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

void free_stack(STACK *s)
{
    free(s->values);
    s->last_index = 0;
}

void init_stack(STACK *s)
{
    s->values = (STACK_VALUE*)malloc(sizeof(STACK_VALUE) * MAX_STACK_SIZE);
    s->last_index = MAX_STACK_SIZE;
}

STACK_VALUE* peek_top(const STACK *s, int i)
{
    assert(s != 0);
    if(i >= 0 && i < FIRST_POSITION && i <= FIRST_POSITION - s->last_index) 
        return &s->values[s->last_index + i];
    else 
        return 0;
}

STACK_VALUE* pop(STACK *s)
{
    assert(s != 0);
    if(s->last_index < MAX_STACK_SIZE) {
        return &s->values[s->last_index++];
    }
    else 
        return 0;
}

bool push(STACK *s, STACK_VALUE v)
{
    assert(s != 0);
    assert(s->values != 0);

    if (s->last_index > 0) {
        s->values[--s->last_index] = v;
        return true;
    }
    else {
        printf("WARNING! too many argument in the stack, make room first!\n");
        return false;
    }
}


void show(const STACK *s)
{
    int i;

    assert(s != 0);
    assert(s->values != 0);

    printf("\n-------------\n");
    for(i = MAX_STACK_SIZE-1; i >= s->last_index; i--) {
        if(s->values[i].is_operation)
            printf("%d = %c\n", i, s->values[i].t.operation);
        else
            printf("%d = %f\n", i, s->values[i].t.value);
    }
    printf("-------------\n");
}

void show_stack_value(const STACK_VALUE *sv) 
{
    if(sv->is_operation)
        printf("%c\n", sv->t.operation);
    else
        printf("%f\n", sv->t.value);
}

int stack_size(const STACK *s)
{
    return MAX_STACK_SIZE - s->last_index;
}
