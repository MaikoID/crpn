#include "../crpn_incl.h"
#include "tests.h"


bool tinput()
{
    CRPN calc;
    init_stack(&calc.stack);
    printf("\n20");
    enter_operand(&calc, 20);
    assert(get(&calc) == 20);
    assert(stack_size(&calc.stack) == 1);

    printf("\n10");
    enter_operand(&calc, 10);
    assert(get(&calc) == 10);
    assert(stack_size(&calc.stack) == 2);

    printf("\n1.5");
    enter_operand(&calc, 1.5);
    assert(get(&calc) == 1.5);
    assert(stack_size(&calc.stack) == 3);

    printf("\n2.5");
    enter_operand(&calc, 2.5);
    show(&calc.stack);
    assert(get(&calc) == 2.5);
    assert(stack_size(&calc.stack) == 4);


    printf("\n+");
    enter_operator(&calc, '+');
    show(&calc.stack);
    assert(stack_size(&calc.stack) == 3);
    assert(get(&calc) == 4.0);

    printf("\n*");
    enter_operator(&calc, '*');
    show(&calc.stack);
    assert(stack_size(&calc.stack) == 2);
    assert(get(&calc) == 40.0);

    printf("\n/");
    enter_operator(&calc, '/');
    show(&calc.stack);
    assert(stack_size(&calc.stack) == 1);
    assert(get(&calc) == 0.5);

    printf("\n-");
    enter_operator(&calc, '-');
    show(&calc.stack);
    assert(stack_size(&calc.stack) == 1);
    assert(get(&calc) == -0.5);

    enter_operand(&calc, 20);
    show(&calc.stack);
    assert(get(&calc) == 20);

    printf("\n-");
    enter_operator(&calc, '-');
    show(&calc.stack);
    assert(stack_size(&calc.stack) == 1);
    assert(get(&calc) == -20.5);

    printf("\n& !Expecting errors!\n");
    enter_operator(&calc, '&');
    show(&calc.stack);
    assert(stack_size(&calc.stack) == 1);
    assert(get(&calc) == -20.5);

    printf("\n0");
    enter_operand(&calc, 0);
    show(&calc.stack);
    assert(stack_size(&calc.stack) == 2);
    assert(get(&calc) == 0);

    printf("\n/ !Expecting errors!\n");
    enter_operator(&calc, '/');
    show(&calc.stack);
    assert(stack_size(&calc.stack) == 2);
    assert(get(&calc) == 0);

    free_stack(&calc.stack);
    return true;
}

bool test_crpn()
{
    printf("\nINPUT TESTS\n");
    return tinput();
}
