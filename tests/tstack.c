#include "../crpn_incl.h"
#include "tests.h"

bool tpush()
{
    STACK s;
    int ret = 0;
    STACK_VALUE sv;

    init_stack(&s);

    sv.is_operation = false;
    sv.t.value = 5.0;
    ret = push(&s, sv);
    assert(ret);
    assert(!s.values[9].is_operation);
    assert(s.values[9].t.value == 5.0);
    show(&s);

    sv.is_operation = false;
    sv.t.value = -30.0;
    ret = push(&s, sv);
    assert(ret);
    assert(!s.values[8].is_operation);
    assert(s.values[8].t.value == -30.0);
    show(&s);

    sv.is_operation = true;
    sv.t.operation = '+';
    ret = push(&s, sv);
    assert(ret);
    assert(s.values[7].is_operation);
    assert(s.values[7].t.operation == '+');

    sv.is_operation = true;
    sv.t.operation = '+';
    ret = push(&s, sv);
    assert(ret);
    assert(s.values[6].is_operation);
    assert(s.values[6].t.operation == '+');

    sv.is_operation = true;
    sv.t.operation = '+';
    ret = push(&s, sv);
    assert(ret);
    assert(s.values[5].is_operation);
    assert(s.values[5].t.operation == '+');

    sv.is_operation = true;
    sv.t.operation = '+';
    ret = push(&s, sv);
    assert(ret);
    assert(s.values[4].is_operation);
    assert(s.values[4].t.operation == '+');

    sv.is_operation = true;
    sv.t.operation = '/';
    ret = push(&s, sv);
    assert(ret);
    assert(s.values[3].is_operation);
    assert(s.values[3].t.operation == '/');

    sv.is_operation = true;
    sv.t.operation = '+';
    ret = push(&s, sv);
    assert(ret);
    assert(s.values[2].is_operation);
    assert(s.values[2].t.operation == '+');

    sv.is_operation = true;
    sv.t.operation = '+';
    ret = push(&s, sv);
    assert(ret);
    assert(s.values[1].is_operation);
    assert(s.values[1].t.operation == '+');
    show(&s);

    sv.is_operation = true;
    sv.t.operation = '*';
    ret = push(&s, sv);
    assert(ret);
    assert(s.values[0].is_operation);
    assert(s.values[0].t.operation == '*');
    show(&s);

    sv.is_operation = false;
    sv.t.value = 20.0;
    ret = push(&s, sv);
    assert(!ret);
    assert(s.values[0].is_operation);
    assert(s.values[0].t.operation == '*');
    show(&s);

    free_stack(&s);
    return true;
}

bool tpop()
{
    STACK s;
    int ret = 0;
    STACK_VALUE sv;
    STACK_VALUE *rsv;

    init_stack(&s);

    show(&s);
    rsv = pop(&s);
    assert(!rsv);

    sv.is_operation = false;
    sv.t.value = 5.0;
    ret = push(&s, sv);
    assert(ret);
    assert(!s.values[9].is_operation);
    assert(s.values[9].t.value == 5.0);

    sv.is_operation = false;
    sv.t.value = -30.0;
    ret = push(&s, sv);
    assert(ret);
    assert(!s.values[8].is_operation);
    assert(s.values[8].t.value == -30.0);

    sv.is_operation = true;
    sv.t.operation = '+';
    ret = push(&s, sv);
    assert(ret);
    assert(s.values[7].is_operation);
    assert(s.values[7].t.operation == '+');
    show(&s);

    rsv = pop(&s);
    show(&s);
    assert(rsv);
    assert(rsv->is_operation);
    assert(rsv->t.operation == '+');

    rsv = pop(&s);
    show(&s);
    assert(rsv);
    assert(!rsv->is_operation);
    assert(rsv->t.value == -30.0);

    rsv = pop(&s);
    show(&s);
    assert(rsv);
    assert(!rsv->is_operation);
    assert(rsv->t.value == 5.0);

    rsv = pop(&s);
    assert(!rsv);

    free_stack(&s);
    return true;
}

bool tpick_top()
{
    STACK s;
    int ret = 0;
    STACK_VALUE sv;
    STACK_VALUE *rsv;

    init_stack(&s);

    show(&s);
    rsv = peek_top(&s, 0);
    assert(!rsv);
    rsv = peek_top(&s, 5);
    assert(!rsv);
    rsv = peek_top(&s, MAX_STACK_SIZE);
    assert(!rsv);
    rsv = peek_top(&s, MAX_STACK_SIZE+1);
    assert(!rsv);
    rsv = peek_top(&s, -1);
    assert(!rsv);

    sv.is_operation = false;
    sv.t.value = 5.0;
    ret = push(&s, sv);
    assert(ret);
    assert(!s.values[9].is_operation);
    assert(s.values[9].t.value == 5.0);

    sv.is_operation = false;
    sv.t.value = -30.0;
    ret = push(&s, sv);
    assert(ret);
    assert(!s.values[8].is_operation);
    assert(s.values[8].t.value == -30.0);

    sv.is_operation = true;
    sv.t.operation = '+';
    ret = push(&s, sv);
    assert(ret);
    assert(s.values[7].is_operation);
    assert(s.values[7].t.operation == '+');
    show(&s);

    rsv = peek_top(&s, 0);
    assert(rsv);
    assert(rsv->is_operation);
    assert(rsv->t.operation == '+');
    printf("peek 0:");
    show_stack_value(rsv);

    rsv = peek_top(&s, 1);
    show(&s);
    assert(rsv);
    assert(!rsv->is_operation);
    assert(rsv->t.value == -30.0);
    printf("peek 1:");
    show_stack_value(rsv);

    rsv = peek_top(&s, 2);
    show(&s);
    assert(rsv);
    assert(!rsv->is_operation);
    assert(rsv->t.value == 5.0);
    printf("peek 2:");
    show_stack_value(rsv);

    rsv = pop(&s);
    show(&s);
    assert(rsv);
    rsv = peek_top(&s, 2);
    assert(!rsv);

    free_stack(&s);
    return true;
}

bool test_stack()
{
    printf("\nSTACK TESTS\n");
    printf("\nPUSH TESTS\n");
    assert(tpush());
    printf("\nPOP TESTS\n");
    assert(tpop());
    printf("\nPICK TESTS\n");
    assert(tpick_top());
    return true;
}

