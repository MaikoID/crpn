#include "crpn_incl.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

int main()
{
    CRPN c;
    init_stack(&c.stack);
    printf("\nReverse Polish Notation Calculator TABAJARA\n");
    printf("supported operations: +, -, /, *\n");
    do {
        show(&c.stack);
        read_console(": ", &c);
    } while (true);
    free_stack(&c.stack);
}
