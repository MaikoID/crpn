/*
 * stack
 *
 *  Created on: Mar 2, 2016
 *      Author: maiko.costa
 */

#ifndef STACK_
#define STACK_

#include <stdbool.h>

#define MAX_STACK_SIZE 10
#define FIRST_POSITION MAX_STACK_SIZE-1

typedef union tokens { 
    double value; 
    char operation;
} TOKEN;

typedef struct stack_values {
    bool is_operation;
    TOKEN t;
} STACK_VALUE;

typedef struct stack {
    STACK_VALUE *values;
    int last_index;
} STACK;

// stack.c
void init_stack(STACK *s);
void free_stack(STACK *s);
bool push(STACK *s, STACK_VALUE v);
STACK_VALUE* pop(STACK *s);
STACK_VALUE* peek_top(const STACK *s, int i);
void show(const STACK *s);
void show_stack_value(const STACK_VALUE *sv);
int stack_size(const STACK *s);

#endif /* STACK_ */
