#ifndef INPUT_
#define INPUT_

#include <stdbool.h>
#include "crpn.h"

bool read_console(char *prompt, CRPN *c);

#endif 
