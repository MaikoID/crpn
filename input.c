#include "input.h"
#include "crpn.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INPUT_LENGTH 255

bool read_console(char *prompt, CRPN *c)
{
    char in[INPUT_LENGTH];
    char *ctrl;
    char op = 0;
    double d;

    printf("%s", prompt);
    fflush(stdout);
    fgets(in, INPUT_LENGTH, stdin);
    if (!strcmp(in, "+\n"))
        op = '+';
    else if (!strcmp(in, "-\n"))
        op = '-';
    else if (!strcmp(in, "*\n"))
        op = '*';
    else if (!strcmp(in, "/\n"))
        op = '/';

    d = strtod(in, &ctrl);
    if(d && *ctrl != '\n')
        return false;

    if (op)
        enter_operator(c, op);
    else if (d)
        enter_operand(c, d);
    else
        return false;

    return true;
}
