#include "crpn.h"
#include "stack.h"

#include <math.h>
#include <float.h>
#include <stdio.h>

static bool get_operands(const CRPN *c, const char *op, double *val0, double *val1);

bool enter_operand(CRPN *c, double val)
{
    STACK_VALUE sv;
    sv.is_operation = false;
    sv.t.value = val;
    return push(&c->stack, sv);
}

bool get_operands(const CRPN *c, const char *op, double *val0, double *val1)
{
    STACK_VALUE *s = peek_top(&c->stack, 0);

    if (!s) {
        // printf("WARNING! Internal error, stack is empty\n");
        return false;
    }
    *val0 = s->t.value;
    if (!isnormal(*val0)) {
        // printf("ERROR! Invalid stack value1: %f\n", *val1);
        return false;
    }

    s = peek_top(&c->stack, 1);
    *val1 = s ? s->t.value : NAN;

    switch (*op) {
        case '-':
            *val1 = !isfinite(*val1) ? 0 : *val1;
            break;
        case '+':
        case '*':
        case '/':
            break;
        default:
            // printf("ERROR! Invalid operation\n");
            return false;
    }

    if (!isfinite(*val1)) {
        // printf("ERROR! Invalid stack value1: %f\n", *val1);
        return false;
    }

    return true;
}

bool enter_operator(CRPN *c, char op)
{
    STACK_VALUE ret;
    double val0 = NAN;
    double val1 = NAN;

    if (!get_operands(c, &op, &val0, &val1)) {
        // printf("ERROR! Invalid stack values\n");
        return false;
    }

    ret.is_operation = false;
    switch (op) {
        case '-':
            ret.t.value = val1 - val0;
            break;
        case '+':
            ret.t.value = val1 + val0;
            break;
        case '*':
            ret.t.value = val1 * val0;
            break;
        case '/':
            if (val0 == 0)
                return false;
            ret.t.value = val1 / val0;
            break;
        default:
            // printf("ERROR! Invalid operation\n");
            return false;
    }
    pop(&c->stack);
    pop(&c->stack);
    return push(&c->stack, ret);
}

double get(CRPN *c)
{
    STACK_VALUE *sv = peek_top(&c->stack, 0);
    return sv ? sv->t.value : NAN;
}

