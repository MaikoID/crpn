#ifndef CRPN_
#define CRPN_

#include "stack.h"

typedef struct crpn {
    STACK stack;
    TOKEN t;
} CRPN;

bool enter_operand(CRPN *c, double val);
bool enter_operator(CRPN *c, char op);
double get(CRPN *c);

#endif /* CRPN_ */
